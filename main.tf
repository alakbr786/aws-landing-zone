data "aws_ami" "Aliakber" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.Aliakber.id
  instance_type = var.ec2_instance_type
  count = var.ec2_count

  tags = {
    Name = var.ec2_name
  }
  
}
resource "aws_ec2_transit_gateway" "aws_ec2_transit_gateway" {

  auto_accept_shared_attachments   = var.auto_accept_shared_attachments


  # Private Autonomous System Number (ASN) for the Amazon side of a BGP session. The range is 64512 to 65534 for 16-bit ASNs and 4200000000 to 4294967294 for 32-bit ASNs. Default value: 64512
  amazon_side_asn                  = var.amazon_side_asn

  # VPN Equal Cost Multipath Protocol
  vpn_ecmp_support                 = var.vpn_ecmp_support

  default_route_table_association  = var.default_route_table_association

  default_route_table_propagation =  var.default_route_table_propagation

  dns_support                      = var.dns_support

  tags                             =  {

    Name                           = var.transit_gateway_name
  }

}


