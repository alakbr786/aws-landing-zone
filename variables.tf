variable "ec2_instance_type" {
    type = string
    default = "t3.medium"  
}

variable "ec2_name" {
    type = string
    default = "🥸😇HelloWorld"
  
}

variable "number_of_instances" {
    type = number
    default = 1
}

variable "ec2_count" {
    type = number
    default = 1
  
}
variable "auto_accept_shared_attachments" {
    type = string
    default = "enable"
  
}

variable "amazon_side_asn" {
    type = string
    default = "65513"
  
}

variable "vpn_ecmp_support" {
    type = string
    default = "enable"
} 

variable "default_route_table_association" {
    type = string
    default = "enable"
  
}

variable "default_route_table_propagation" {
    type = string
    default = "enable"
  
}

variable "dns_support" {
    type = string
    default = "disable"

}

variable "transit_gateway_name" {
    type = string
    default = "tgw"
  
}
  
